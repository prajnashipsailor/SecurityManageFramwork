# -*- coding: utf-8 -*-
# @Time    : 2020/11/23
# @Author  : canyuan
# @Email   : gy071089@outlook.com
# @File    : typeviews.py

from django.http import JsonResponse
from rest_framework.decorators import api_view
from Base.Functions.basefun import xssfilter
from .. import models, serializers


@api_view(['GET'])
def select_views(request):
    data = {
        "code": 1,
        "msg": "",
        "count": '',
        "data": []
    }
    key = request.GET.get('key', '')
    list_get = models.Type.objects.filter(name__icontains=key).order_by('-id')
    list_count = list_get.count()
    serializers_get = serializers.TypeSelectSerializer(instance=list_get, many=True)
    data['code'] = 0
    data['msg'] = 'success'
    data['count'] = list_count
    data['data'] = xssfilter(serializers_get.data)
    return JsonResponse(data)
