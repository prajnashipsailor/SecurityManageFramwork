# -*- coding: utf-8 -*-
# @Time    : 2020/5/12
# @Author  : canyuan
# @Email   : gy071089@outlook.com
# @File    : serializers.py

from rest_framework import serializers
from . import models


class LogSerializer(serializers.ModelSerializer):
    create_time = serializers.DateTimeField(format='%Y-%m-%d %H:%M')

    class Meta:
        model = models.Log
        fields = ('type', 'action', 'ip', 'status', 'create_time')


class LogAllSerializer(serializers.ModelSerializer):
    create_time = serializers.DateTimeField(format='%Y-%m-%d %H:%M')
    user = serializers.CharField(source='user.username')

    class Meta:
        model = models.Log
        fields = ('type', 'user', 'ip', 'action', 'status', 'create_time')
