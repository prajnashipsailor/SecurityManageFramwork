# -*- coding: utf-8 -*-
# @Time    : 2020/11/10
# @Author  : canyuan
# @Email   : gy071089@outlook.com
# @File    : languageviews.py

from django.http import JsonResponse
from rest_framework.decorators import api_view
from Base.Functions.basefun import xssfilter
from .. import models, serializers


@api_view(['GET'])
def select_language_views(request):
    data = {
        "code": 0,
        "msg": "",
        "count": '',
        "data": []
    }
    list_get = models.LanguageType.objects.all()
    list_count = list_get.count()
    serializers_get = serializers.LanguageTypeSerializer(instance=list_get, many=True)
    data['msg'] = 'success'
    data['count'] = list_count
    data['data'] = xssfilter(serializers_get.data)
    return JsonResponse(data)