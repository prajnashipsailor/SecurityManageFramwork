# -*- coding: utf-8 -*-
# @Time    : 2020/6/24
# @Author  : canyuan
# @Email   : gy071089@outlook.com
# @File    : forms.py
from django.forms import ModelForm
from . import models
from django import forms


class DepartmentForm(ModelForm):
    class Meta:
        model = models.Department
        fields = ('name', 'parent')


class AreaForm(ModelForm):
    class Meta:
        model = models.Area
        fields = ('name', 'description')


class PersonForm(ModelForm):
    class Meta:
        model = models.Person
        fields = ('name', 'mail', 'is_valid', 'dep')


class ClientForm(ModelForm):
    class Meta:
        model = models.Client
        fields = ('ip', 'mac', 'des', 'person')


class DomainForm(ModelForm):
    class Meta:
        model = models.Domain
        fields = ('key', 'description', 'manage', 'start_date', 'end_date')


class VlanForm(ModelForm):
    class Meta:
        model = models.Vlan
        fields = ('key', 'description', 'manage')


class PermissionForm(ModelForm):
    class Meta:
        model = models.PermissionManage
        fields = ('department', 'area', 'description')


class U2p_Form(forms.Form):
    username = forms.CharField(label='账号', max_length=75)
    password = forms.CharField(label='密码', max_length=25)
