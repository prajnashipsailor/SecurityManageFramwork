# -*- coding: utf-8 -*-
# @Time    : 2020/6/24
# @Author  : canyuan
# @Email   : gy071089@outlook.com
# @File    : domain.py

from django.http import JsonResponse
from rest_framework.decorators import api_view
from Base.Functions.basefun import MyPageNumberPagination, xssfilter
from .. import models, forms
from .. import serializers
from django.views.decorators.csrf import csrf_protect


@api_view(['GET'])
def vlan_list(request):
    data = {
        "code": 1,
        "msg": "",
        "count": '',
        "data": []
    }
    key = request.GET.get('key','')
    user = request.user
    if user.is_superuser:
        list_get = models.Vlan.objects.filter(key__icontains=key).order_by('updatetime')
        list_count = list_get.count()
        pg = MyPageNumberPagination()
        list_page = pg.paginate_queryset(list_get, request, 'self')
        serializers_get = serializers.VlanSerializer(instance=list_get, many=True)
        data['code'] = 0
        data['msg'] = 'success'
        data['count'] = list_count
        data['data'] = xssfilter(serializers_get.data)
    else:
        data['msg'] = '权限不足'
    return JsonResponse(data)


@api_view(['GET'])
def vlan_delete(request, vlan_id):
    data = {
        "code": 1,
        "msg": "",
        "count": '',
        "data": []
    }
    user = request.user
    if user.is_superuser:
        item_get = models.Vlan.objects.filter(id=vlan_id).first()
        if item_get:
            item_get.delete()
            data['code'] = 0
            data['msg'] = '操作成功'
        else:
            data['msg'] = '你要干啥'
    else:
        data['msg'] = '权限不足'
    return JsonResponse(data)


@api_view(['POST'])
@csrf_protect
def vlan_create(request):
    data = {
        "code": 1,
        "msg": "",
        "count": '',
        "data": []
    }
    user = request.user
    if user.is_superuser:
        form = forms.VlanForm(request.POST)
        if form.is_valid():
            form.save()
            data['code'] = 0
            data['msg'] = '添加成功'
        else:
            data['msg'] = '请检查参数'
    else:
        data['msg'] = '权限不足'
    return JsonResponse(data)


@api_view(['POST'])
@csrf_protect
def vlan_update(request, vlan_id):
    data = {
        "code": 1,
        "msg": "",
        "count": '',
        "data": []
    }
    user = request.user
    if user.is_superuser:
        vlan_get = models.Vlan.objects.filter(id=vlan_id).first()
        if vlan_get:
            form = forms.VlanForm(request.POST, instance=vlan_get)
            if form.is_valid():
                form.save()
                data['code'] = 0
                data['msg'] = '添加成功'
            else:
                data['msg'] = '请检查参数'
        else:
            data['msg'] = '指定参数不存在'
    else:
        data['msg'] = '权限不足'
    return JsonResponse(data)
