# -*- coding: utf-8 -*-
# @Time    : 2020/11/16
# @Author  : canyuan
# @Email   : gy071089@outlook.com
# @File    : urls.py
from django.urls import path
from .views import views


urlpatterns = [
    path('user/list/', views.list_views),
    path('user/deny/<str:user_id>/', views.deny_views),
    path('user/create/', views.create_views),
]