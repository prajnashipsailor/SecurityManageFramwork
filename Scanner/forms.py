# -*- coding: utf-8 -*-
# @Time    : 2020/7/10
# @Author  : canyuan
# @Email   : gy071089@outlook.com
# @File    : forms.py


from django.forms import ModelForm
from . import models
from django import forms


class ScannerEditForm(ModelForm):
    class Meta:
        model = models.Scanner
        fields = ('name', 'type', 'url', 'status', 'apikey', 'engine','description')


class PoliciesEditForm(ModelForm):
    class Meta:
        model = models.Policies
        fields = ('name', 'key', 'description')

