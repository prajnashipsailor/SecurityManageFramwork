# -*- coding: utf-8 -*-
# @Time    : 2020/7/10
# @Author  : canyuan
# @Email   : gy071089@outlook.com
# @File    : serializers.py
from rest_framework import serializers
from . import models


class ScannerSerializer(serializers.ModelSerializer):
    addtime = serializers.DateTimeField(format='%Y-%m-%d %H:%M')
    updatetime = serializers.DateTimeField(format='%Y-%m-%d %H:%M')

    class Meta:
        model = models.Scanner
        fields = ('id', 'type', 'name', 'url', 'status', 'engine', 'description', 'addtime', 'updatetime')


class PoliceSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.Policies
        fields = "__all__"
