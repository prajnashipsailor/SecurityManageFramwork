# coding:utf-8
from django.db import models


# Create your models here.
class EchoLog(models.Model):
    path = models.CharField(u'访问路径', max_length=50)
    key = models.CharField(u'键值', max_length=100)
    ip = models.CharField(u'终端IP', max_length=30)
    ua = models.TextField(u'终端UA')
    status = models.NullBooleanField('是否失效', default=False)
    create_time = models.DateTimeField('操作时间', auto_now_add=True)

    def __str__(self):
        return str(self.key)
