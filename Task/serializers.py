# -*- coding: utf-8 -*-
# @Time    : 2020/6/2
# @Author  : canyuan
# @Email   : gy071089@outlook.com
# @File    : serializers.py


from rest_framework import serializers
from . import models
from Workflow.models import Tickets


class TaskListSerializer(serializers.ModelSerializer):
    status = serializers.SerializerMethodField('get_status')
    type = serializers.SerializerMethodField('get_type')
    person = serializers.SerializerMethodField('get_person')
    user = serializers.SerializerMethodField('get_user')
    manage = serializers.SerializerMethodField('get_manage')
    asset = serializers.SerializerMethodField('get_asset')
    ticket_id = serializers.SerializerMethodField('get_ticket_id')

    class Meta:
        model = models.Task
        fields = (
        'id', 'name', 'target', 'type', 'status', 'person', 'manage', 'description', 'user', 'asset', 'ticket_id', 'cycle_hours', 'start_time')

    def get_asset(self, obj):
        asset_list = []
        for item in obj.asset.all():
            asset_list.append({'id': item.id, 'name': item.key})
        return asset_list

    def get_type(self, obj):
        try:
            return {'id': obj.type.id, 'name': obj.type.name}
        except:
            return None

    def get_user(self, obj):
        try:
            return {'id': obj.user.id, 'name': obj.user.name}
        except:
            return None

    def get_person(self, obj):
        try:
            return {'id': obj.person.id, 'name': obj.person.name}
        except:
            return None

    def get_manage(self, obj):
        manage_list = []
        for item in obj.manage.all():
            manage_list.append({'id': item.id, 'name': item.name})
        return manage_list

    def get_ticket_id(self, obj):
        ticket_obj = Tickets.objects.filter(ticket_key='task', ticket_id=obj.id).first()
        if ticket_obj:
            return ticket_obj.id
        return None

    def get_status(self, obj):
        ticket_obj = Tickets.objects.filter(ticket_key='task', ticket_id=obj.id).first()
        if ticket_obj:
            return ticket_obj.workflow.status
        else:
            return '未知'


class TypeSelectSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.Type
        fields = ('id', 'name')


class TaskDetailsSerializer(serializers.ModelSerializer):
    type = serializers.SerializerMethodField('get_type')
    user = serializers.SerializerMethodField('get_user')
    person = serializers.SerializerMethodField('get_person')
    manage = serializers.SerializerMethodField('get_manage')
    asset = serializers.SerializerMethodField('get_asset')
    ticket_id = serializers.SerializerMethodField('get_ticket_id')

    class Meta:
        model = models.Task
        fields = '__all__'

    def get_asset(self, obj):
        asset_list = []
        for item in obj.asset.all():
            asset_list.append({'id': item.id, 'name': item.key})
        return asset_list

    def get_type(self, obj):
        try:
            return {'id': obj.type.id, 'name': obj.type.name}
        except:
            return None

    def get_user(self, obj):
        try:
            return {'id': obj.user.id, 'name': obj.user.name}
        except:
            return None

    def get_person(self, obj):
        try:
            return {'id': obj.person.id, 'name': obj.person.name}
        except:
            return None

    def get_manage(self, obj):
        manage_list = []
        for item in obj.manage.all():
            manage_list.append({'id': item.id, 'name': item.name})
        return manage_list

    def get_ticket_id(self, obj):
        ticket_obj = Tickets.objects.filter(ticket_key='task', ticket_id=obj.id).first()
        if ticket_obj:
            return ticket_obj.id
        return None


class TaskScanSerializer(serializers.ModelSerializer):
    police = serializers.CharField(source='police.name')
    addtime = serializers.DateTimeField(format='%Y-%m-%d')

    class Meta:
        model = models.TaskScan
        fields = "__all__"


class PreDefineSerializer(serializers.ModelSerializer):
    tasktype = serializers.SerializerMethodField('get_tasktype')
    police = serializers.SerializerMethodField('get_police')
    updatetime = serializers.DateTimeField(format='%Y-%m-%d')

    class Meta:
        model = models.PreDefine
        fields = "__all__"

    def get_police(self, obj):
        police_list = obj.police.all().first()
        try:
            return {'id':police_list.id, 'name': police_list.name}
        except:
            return None

    def get_tasktype(self, obj):
        try:
            return {'id':obj.tasktype.id, 'name':obj.tasktype.name}
        except:
            return None

