# coding:utf-8
from django.contrib import admin
from . import models

# Register your models here.
admin.site.register(models.Task)
admin.site.register(models.Type)
admin.site.register(models.PreDefine)
admin.site.register(models.TaskScan)