# -*- coding: utf-8 -*-
# @Time    : 2020/6/16
# @Author  : canyuan
# @Email   : gy071089@outlook.com
# @File    : serializers.py
from rest_framework import serializers
from . import models


class ExterNameField(serializers.CharField):
    def to_representation(self, value):
        if value:
            return value.username
        else:
            return None


class PocSerializer(serializers.ModelSerializer):
    create_data = serializers.DateTimeField(format='%Y-%m-%d %H:%M')
    update_data = serializers.DateTimeField(format='%Y-%m-%d %H:%M')
    user = ExterNameField()
    update_user = ExterNameField()

    class Meta:
        model = models.POC
        fields = ('id', 'name', 'key', 'level', 'introduce', 'user', 'update_user', 'create_data', 'update_data')


class PocDetailsSerializer(serializers.ModelSerializer):
    create_data = serializers.DateTimeField(format='%Y-%m-%d %H:%M')
    update_data = serializers.DateTimeField(format='%Y-%m-%d %H:%M')

    class Meta:
        model = models.POC
        fields = '__all__'
